# -*- coding: utf-8 -*-
from tkinter import *
from tkinter.ttk import Progressbar, Checkbutton
from tkinter import ttk
from difflib import get_close_matches
from re import finditer
from time import sleep

class Application(Frame):
    def __init__(self, master=None):
        self.percent=0
        self.count_min=0
        self.count_max=0
        self.logs_msg="Znalezione\n słowa"
        self.out="Wyniki"
        self.data_got=False
        self.data_got_script=False
        self.printed=False

        self.words=[]
        self.questions=[]
        self.data=[]
        self.close_matches=[]
        self.answer_nrs=[]
        self.answer_words=[]

        ##
        self.original=""
        self.idx_swords=[]
        self.swords=[]
        self.sclose_matches=[]
        self.targets=[]
        self.target_words=[]
        self.location=-1
        ##

        Frame.__init__(self, master)
        root.bind("<Return>",self.button_out)
        self.pack()
        self.checkstate=BooleanVar()
        self.selected = IntVar()
        self.selected.set(1)
        self.checkstate.set(True)
        self.createWidgets()
    def reset(self):

        self.sclose_matches=[]
        self.targets=[]
        self.target_words=[]
        self.location=-1

        self.percent=0
        self.count_min=0
        self.count_max=0
        self.logs_msg="Znalezione\n słowa"
        self.out="Wyniki"
        self.printed=False

        #self.words=[]
        #self.questions=[]
        self.data=[]
        self.close_matches=[]
        self.answer_nrs=[]
        self.answer_words=[]

        self.update_bar(0)
        self.update_count_msg()
        self.update_logs("Znalezione\n słowa")
        self.update_output("Wyniki")
    def createWidgets(self):
        self.input_desc=Label(root,text="Wyszukaj:",bd=0,bg="white")
        self.input_desc.place(x=5,y=10)

        self.input_=Entry(root, width=30)
        self.input_.place(x=65,y=10)

        self.tickmark = Checkbutton(root, text="Szukaj w odpowiedziach\n(dot. tylko pytań)",var=self.checkstate)
        self.tickmark.place(x=305,y=5)

        self.output=Label(root,text=self.out,fg="black",bg="white", width=40,height=10, font=("TkDefaultFont",13),wraplength=350)
        #self.output=Label(root,text=self.out,fg="black",bg="white", width=45,height=12, font=("TkDefaultFont",10),wraplength=350)
        self.output.place(x=5,y=50)

        self.logs=Label(root,text=self.logs_msg,fg="black",bg="white", width=22,height=10, font=("TkDefaultFont",13),wraplength=150)
        self.logs.place(x=380,y=50)

        self.bar = Progressbar(root, length=200, style='green.Horizontal.TProgressbar')
        self.bar.place(x=380,y=250)

        self.count_msg=Label(root, text="Wynik: 0/0")
        self.count_msg.place(x=205,y=252)

        self.button_next = Button(root,bg="red",activebackground="blue",bd=1,text="Następny wynik", command=self.button_next)
        self.button_next.place(x=105,y=250)

        self.button_prev = Button(root,bg="red",activebackground="blue",bd=1,text="Poprzedni wynik", command=self.button_prev)
        self.button_prev.place(x=5,y=250)

        self.button = Button(root,bg="red",activebackground="blue",bd=1,text="Szukaj", command=self.button_out)
        self.button.place(x=250,y=10)

        self.button_up = Button(root,bg="red",activebackground="blue",bd=1,text="-->", command=self.forward_txt)
        self.button_up.place(x=345,y=250)

        self.button_down = Button(root,bg="red",activebackground="blue",bd=1,text="<--", command=self.backward_txt)
        self.button_down.place(x=320,y=250)

        self.rad1 = Radiobutton(root,text='Pytania', value=1, variable=self.selected,command=self.change_label)
        self.rad2 = Radiobutton(root,text='Skrypt', value=2, variable=self.selected,command=self.change_label)
        self.rad1.place(x=455,y=11)
        self.rad2.place(x=520,y=11)

        self.input_.focus()
    def forward_txt(self):
        if self.location==-1: return 0
        self.location+=30
        begin=self.location-30
        end=self.location+30
        if begin<0:begin=0
        if end>len(self.original):end=len(self.original)
        self.update_output(" ".join(self.original[begin:end]))

    def backward_txt(self):
        if self.location==-1: return 0
        self.location-=30
        begin=self.location-30
        end=self.location+30
        if begin<0:begin=0
        if end>len(self.original):end=len(self.original)
        self.update_output(" ".join(self.original[begin:end]))

    def change_label(self):
        self.reset()
        if self.selected.get()==2: self.output.configure(text=self.out,fg="black",bg="white", width=45,height=12, font=("TkDefaultFont",10),justify="left")
        else: self.output.configure(text=self.out,fg="black",bg="white", width=40,height=10, font=("TkDefaultFont",13),justify="center")
        self.output.update()

    def update_count_msg(self): self.count_msg['text']="Wynik: "+'/'.join([str(self.count_min),str(self.count_max)])
    def update_logs(self,msg,extend=False):
        if not extend:
            self.logs_msg=msg
        else:
            self.logs_msg+=msg
        self.logs['text']=self.logs_msg
        self.logs.update()

    def update_output(self,msg,extend=False):
        if not extend:
            self.out=msg
        else:
            self.out+=msg
        self.output['text']=self.out

    def update_bar(self,p):
        p=int(p)
        if p!=0: self.percent+=p
        elif p==100: self.percent=100
        else: self.percent=0
        self.bar['value'] = int(self.percent)

    def button_next(self):
        if self.printed:
            self.count_min+=1
            if self.count_min>self.count_max:
                self.count_min=self.count_max
            self.update_count_msg()


            if self.selected.get()==2:
                idx=self.targets[self.count_min-1][0]
                begin=idx-60
                end=idx+60
                if begin<0:begin=0
                if end>len(self.original):
                    end=len(self.original)
                if idx==len(self.original): idx=len(self.original)-1

                out=""
                for i in range(begin,end+1):
                    try:
                        if i not in self.targets[self.count_min-1]:
                            out+=" "+self.original[i]+" "
                        else:
                            out+=" *"+self.original[i]+"* "
                    except:
                        if i!=idx:
                            out+=" "+self.original[i]+" "
                        else:
                            out+=" *"+self.original[i]+"* "
                self.update_output(out)
                self.update_logs("Znalezione dla:\n"+" ".join(self.target_words[self.count_min-1]))
                self.location=idx

            else:
                self.update_output("".join(self.questions[self.answer_nrs[self.count_min-1]]))
                self.update_logs("Znalezione dla:\n"+" ".join(self.answer_words[self.count_min-1]))

    def button_prev(self):
        if self.printed:
            self.count_min-=1
            if self.count_min<1:
                self.count_min=1
            self.update_count_msg()

            if self.selected.get()==2:
                idx=self.targets[self.count_min-1][0]
                begin=idx-60
                end=idx+60
                if begin<0:begin=0
                if end>len(self.original):
                    end=len(self.original)
                if idx==len(self.original): idx=len(self.original)-1

                out=""
                for i in range(begin,end+1):
                    try:
                        if i not in self.targets[self.count_min-1]:
                            out+=" "+self.original[i]+" "
                        else:
                            out+=" *"+self.original[i]+"* "
                    except:
                        if i!=idx:
                            out+=" "+self.original[i]+" "
                        else:
                            out+=" *"+self.original[i]+"* "
                self.update_output(out)
                self.update_logs("Znalezione dla:\n"+" ".join(self.target_words[self.count_min-1]))
                self.location=idx

            else:
                self.update_output("".join(self.questions[self.answer_nrs[self.count_min-1]]))
                self.update_logs("Znalezione dla:\n"+" ".join(self.answer_words[self.count_min-1]))

    def button_out(self,event=None):
        if self.input_.get()!="":
            self.run()

    def sleepy(self,short=False):
        root.update_idletasks()
        if not short: sleep(0.02)
        else: return 1

    def run(self):
        if self.selected.get()==2:
            self.run_scriptsearch()
            return 1
        self.reset()
        self.update_bar(0)
        self.get_input_data()
        self.update_bar(10)
        self.sleepy()
        if(self.get_data()==0):
            self.update_bar(0)
            return 0
        self.update_bar(10)
        self.sleepy()

        self.get_close_words()
        self.sleepy()

        if(self.find_matches()==0):
            self.update_bar(0)
            return 0
        self.update_bar(20)

        self.print_results()
        self.update_bar(100)

    def run_scriptsearch(self):
        self.reset()
        self.update_bar(0)
        self.get_input_data()
        self.update_bar(10)
        self.sleepy(True)
        if(self.get_data_script()==0):
            self.update_bar(0)
            return 0
        self.update_bar(10)
        self.sleepy(True)

        self.get_close_words_script()
        self.sleepy(True)

        if(self.find_matches_script()==0):
            self.update_bar(0)
            return 0
        self.update_bar(20)

        self.print_results_script()
        self.update_bar(100)

    def get_data(self):
        if(self.data_got):
            return 1
        _=""
        try:
            file =open("data.txt","r")
            _=file.readlines()
            file.close()
        except:
            self.update_logs("Nie znaleziono pliku!\n Dodaj data.txt do folderu\n i wyszukaj ponownie")
            return 0
        self.update_logs("Szukam...")
        self.logs.update()
        file =open("data.txt","r")
        words=file.read()
        file.close()

        file =open("data.txt","r")
        questions=[0]*len(_)

        words=words.lower().strip().replace(","," ").replace("."," ").replace("\n"," ").replace("\t"," ").replace("-"," ").replace("("," ").replace(")"," ").replace(":"," ").replace('„'," ").replace("”"," ")
        words=list(filter(None,words.split(" ")))

        idx=-1
        while True:
            f=file.readline()

            if f=="":break
            if f==f.upper(): continue
            if f[0].isnumeric():
                idx+=1
                questions[idx]=[f]
                continue
            questions[idx].append(f)

        try:
            questions=questions[:questions.index(0)]
        except: None
        self.words=words
        self.questions=questions
        self.data_got=True

        file.close()
    def clean(self,word_):
        word=word_
        return word.lower().strip().replace(","," ").replace("."," ").replace("\n"," ").replace("\t"," ").replace("-"," ").replace("("," ").replace(")"," ").replace(":"," ").replace('„'," ").replace("”"," ").strip()
    def get_data_script(self):
        if(self.data_got_script):
            return 1
        try:
            file =open("data_2.txt","r")
            self.original=file.read()
            file.close()
        except:
            self.update_logs("Nie znaleziono pliku!\n Dodaj data_2.txt do folderu\n i wyszukaj ponownie")
            return 0
        self.update_logs("Szukam...")
        self.logs.update()

        self.original=self.original.split(" ")
        for i in range(0,len(self.original)):
            self.idx_swords.append(i)
            self.swords.append(self.clean(self.original[i]))

        while True:
            try:
                tmp=self.swords.index("")
                self.swords.pop(tmp)
                self.idx_swords.pop(tmp)
            except:
                break
        self.data_got_script=True
        self.update_logs("Zebrano i przefiltrowano tekst...")

    def get_input_data(self):
        data=[]
        d=self.input_.get().lower()
        try:
            d.index(",")
            d.index(" ")
            data=d.split(", ")
        except:
            try:
                d.index(",")
                data=d.split(",")
            except:
                try:
                    d.index(" ")
                    data=' '.join(d.split())
                    data=data.split(" ")
                except:
                    data=[d]
        self.data=data

    def get_close_words(self):
        #self data - input, self words - slowa z dokumentu, self questions - pytania
        arr=[0]*len(self.data)
        out=[]
        for i in range(len(self.data)):
            self.update_bar(30/len(self.data))
            self.sleepy(True)
            out.append([])
            arr[i]=get_close_matches(self.data[i],self.words,10000,0.4)
            for entry in arr[i]:
                if entry not in out[i] and len(out[i])<=7:
                    out[i].append(entry)

        for i in range(len(out)):
            if out[i]==[]:
                out.pop(i)

        self.update_bar(10)
        self.close_matches=out
    def get_close_words_script(self):
        #self data - input, self words - slowa z dokumentu, self questions - pytania
        arr=[0]*len(self.data)
        out=[]
        for i in range(len(self.data)):
            self.update_bar(30/len(self.data))
            self.sleepy(True)
            out.append([])
            arr[i]=get_close_matches(self.data[i],self.swords,10000,0.4)
            for entry in arr[i]:
                if entry not in out[i] and len(out[i])<=7:
                    out[i].append(entry)

        for i in range(len(out)):
            if out[i]==[]:
                out.pop(i)

        self.update_bar(10)
        self.sclose_matches=out

    def clear(self,w):
        w=w.lower().strip().replace(","," ").replace("."," ").replace("\n"," ").replace("\t"," ").replace("-"," ").replace("("," ").replace(")"," ").replace(":"," ").replace('„'," ").replace("”"," ")
        w=list(filter(None,w.split(" ")))
        return w

    def find_matches(self):
        for idx,q in enumerate(self.questions):

            i=0

            if(self.checkstate.get()==True):
                q=" ".join(q)
                q=self.clear(q)
            else:
                tmp=[]
                for s in q:
                    if not(s[0].isupper() and s[1]=="."):
                        tmp.append(s)
                q=" ".join(tmp)
                q=self.clear(q)

            found=True
            correct_words=[]
            while(i!=len(self.close_matches)):
                correct=False
                for x in self.close_matches[i]:
                    if x in q:
                        i+=1
                        correct=True
                        correct_words.append(x)
                        break
                if not correct:
                    found=False
                    break
            if found:
                self.answer_nrs.append(idx)
                self.answer_words.append(correct_words)
        if len(self.answer_nrs)==0:
            self.update_logs("Nie znaleziono pasujących pytań :(")
            return 0
        else: return 1
    def find_matches_script(self):

        search_range=20

        for iii,q in enumerate(self.swords):
            idx=self.idx_swords[iii]

            if q in self.sclose_matches[0]:
                if len(self.sclose_matches)>1:
                    tg_words=[q]
                    tgs=[idx]
                    begin=iii-search_range
                    end=iii+search_range
                    _=len(self.swords)
                    if begin<0:begin=0
                    if end>_:end=_

                    x=1
                    while x<len(self.sclose_matches):
                        found=False

                        for i in range(begin,end):
                            w=self.swords[i]
                            if w in self.sclose_matches[x]:
                                x+=1
                                tg_words.append(w)
                                tgs.append(self.idx_swords[i])
                                found=True
                                break
                        if found:
                            continue
                        else:
                            break
                    if x!=len(self.sclose_matches):
                        continue
                    else:
                        self.targets.append(tgs)
                        self.target_words.append(tg_words)
                else:
                    self.targets.append([idx])
                    self.target_words.append([q])


        if len(self.targets)==0:
            self.update_logs("Nie znaleziono pasujących pytań :(")
            return 0
        else:
            return 1

    def print_results(self):
        self.printed=True
        self.count_min=1
        self.count_max=len(self.answer_nrs)
        self.update_count_msg()
        self.update_output("".join(self.questions[self.answer_nrs[self.count_min-1]]))
        self.update_logs("Znalezione dla:\n"+" ".join(self.answer_words[self.count_min-1]))

    def print_results_script(self):
        self.printed=True
        self.count_min=1
        self.count_max=len(self.targets)
        self.update_count_msg()
        idx=self.targets[self.count_min-1][0]
        begin=idx-50
        end=idx+50
        if begin<0:begin=0
        if end>len(self.original):
            end=len(self.original)
        if idx==len(self.original): idx=len(self.original)-1

        out=""
        for i in range(begin,end+1):
            try:
                if i not in self.targets[self.count_min-1]:
                    out+=" "+self.original[i]+" "
                else:
                    out+=" *"+self.original[i]+"* "
            except:
                if i!=idx:
                    out+=" "+self.original[i]+" "
                else:
                    out+=" *"+self.original[i]+"* "

        self.update_output(out)
        self.update_logs("Znalezione dla:\n"+" ".join(self.target_words[self.count_min-1]))
        self.location=idx

root = Tk()
root.title("Super Searcher")
#root.iconbitmap('icon.ico')
root.focus_set()
root.geometry("600x300")
app = Application(master=root)
app.mainloop()
